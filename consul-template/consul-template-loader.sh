#!/bin/sh
#
# Author: Rodrigo Zampieri Castilho <rodrigo.zampieri@gmail.com>
#
# Build Consul Template parameters from all template definitions
#

CONFIG_FILE=$1
CONFIG_HOME=/etc/consul-template

if [ -e ${CONFIG_FILE} ]; then
  rm -f ${CONFIG_FILE}
fi

for CONFIG in `find $CONFIG_HOME -name '*.list' | sort`
do
  while read TEMPLATE
  do
    SOURCE=`echo $TEMPLATE | awk 'BEGIN { FS=":"; } { print $1; }'`
    DESTINATION=`echo $TEMPLATE | awk 'BEGIN { FS=":"; } { print $2; }'`
    COMMAND=`echo $TEMPLATE | awk 'BEGIN { FS=":"; } { print $3; }'`
    echo "template {" >> ${CONFIG_FILE}
    echo "  source = \"${SOURCE}\"" >> ${CONFIG_FILE}
    echo "  destination = \"${DESTINATION}\"" >> ${CONFIG_FILE}
    if [ -n "$COMMAND" ]; then
      echo "  command = \"$COMMAND\"" >> ${CONFIG_FILE}
    fi
    echo "}" >> ${CONFIG_FILE}
  done < $CONFIG
done
