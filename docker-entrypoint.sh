#!/usr/bin/dumb-init /bin/sh
set -e

# Configure environment
/config/configure_environment.sh

# Start all scripts in /docker/run
for COMMAND in `find /docker/run -maxdepth 1 -name '*.sh' | sort`; do
  $COMMAND
done

# Start consul
exec dumb-init /consul.sh
